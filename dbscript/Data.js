const mongoose = require('mongoose')
const ClassroomBookingCertificate = require('../models/ClassroomBookingCertificate')
const Room = require('../models/Room')
const Building = require('../models/Building')
const User = require('../models/User')
const Admin = require('../models/Admin')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/Classroom_Reservations_Record')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
  await User.deleteMany({})
  await Admin.deleteMany({})
  await ClassroomBookingCertificate.deleteMany({})
}

async function main () {
  await clearDb()
  // await clearUser()
  // await clear()

  const informaticsBuilding = new Building({ name: 'Informatics', floor: 11 })
  // Room 3
  const room3c01 = new Room({ name: 'IF-3c01', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c01)
  const room3c02 = new Room({ name: 'IF-3c02', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c02)
  const room3c03 = new Room({ name: 'IF-3c03', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c03)
  const room3c04 = new Room({ name: 'IF-3c04', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c04)
  const room3m210 = new Room({ name: 'IF-3m210', capacity: 210, floor: 3, descs: ['Meeting room'], approver_order: 1, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3m210)
  await informaticsBuilding.save()
  await room3c01.save()
  await room3c02.save()
  await room3c03.save()
  await room3c04.save()
  await room3m210.save()

  // Room 4
  const room4c01 = new Room({ name: 'IF-3c01', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c01)
  const room4c02 = new Room({ name: 'IF-3c02', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c02)
  const room4c03 = new Room({ name: 'IF-3c03', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c03)
  const room4c04 = new Room({ name: 'IF-3c04', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c04)
  const room4m210 = new Room({ name: 'IF-3m210', capacity: 210, floor: 4, descs: ['Meeting room'], approver_order: 1, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4m210)
  await informaticsBuilding.save()
  await room4c01.save()
  await room4c02.save()
  await room4c03.save()
  await room4c04.save()
  await room4m210.save()

  // Room for Test
  // const roomTest = new Room({ name: 'roomTest01', capacity: 60, floor: 4, descs: ['TestObject1', 'TestObject2', 'TestObject3'], approver_order: 2, building: informaticsBuilding })
  // informaticsBuilding.rooms.push(roomTest)
  // await informaticsBuilding.save()
  // await roomTest.save()

  const user1 = new User({ username: '62160244@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0864879521', roles: [ROLE.USER] })
  user1.save()

  const user2 = new User({ username: '62160252@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0698752149', roles: [ROLE.USER] })
  user2.save()

  const user3 = new User({ username: '62160277@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0875414785', roles: [ROLE.USER] })
  user3.save()

  const user4 = new User({ username: '62160310@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0698754124', roles: [ROLE.USER] })
  user4.save()

  const user5 = new User({ username: '62160315@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0647828457', roles: [ROLE.USER] })
  user5.save()

  const institutioAdministrator = new Admin({ username: 'admin1@mail.com', password: 'password', roles: [ROLE.INSTITUTIOAM] }) // ดูแลหน่วยงาน
  institutioAdministrator.save()

  const systemAdministrator = new Admin({ username: 'admin2@mail.com', password: 'password', roles: [ROLE.SYSTEM] })// ดูแลระบบ
  systemAdministrator.save()

  const approver1 = new Admin({ username: 'approver1@mail.com', password: 'password', roles: [ROLE.APPROVER] })//  พิจารณา1
  approver1.save()

  const approver2 = new Admin({ username: 'approver2@mail.com', password: 'password', roles: [ROLE.APPROVER] })//  พิจารณา2
  approver2.save()

  // Get time in DB => new Date("<YYYY-mm-ddTHH:MM:ss>")
  // 0 false && 1 true
  await ClassroomBookingCertificate.insertMany([
    {
      room: 'IF-3c01',
      building: 'informatics',
      startDate: '08:00 ',
      endDate: '16:00 ',
      user: '62160244@go.buu.ac.th',
      approver: 'approver1@mail.com',
      bcStatus: 'Awaiting approval',
      note: 'I need Mac Book'
    },
    {
      room: 'IF-3c02',
      building: 'informatics',
      startDate: '08:00 ',
      endDate: '16:00 ',
      user: '62160244@go.buu.ac.th',
      approver: 'approver1@mail.com',
      bcStatus: 'Awaiting approval',
      note: 'I need window 11'
    },
    {
      room: 'IF-3c03',
      building: 'informatics',
      startDate: '08:00 ',
      endDate: '16:00 ',
      user: '62160244@go.buu.ac.th',
      approver: 'approver1@mail.com',
      bcStatus: 'Awaiting approval',
      note: 'I need linux '
    }
  ])
}

main().then(function () {
  console.log('Finish')
})
