const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/Classroom_Reservations_Record')
// eslint-disable-next-line no-unused-vars
const ClassroomBookingCertificate = require('../models/ClassroomBookingCertificate')
const Room = require('../models/Room')
const Building = require('../models/Building')

async function clearDb () {
  await Room.deleteMany({})
  await Building.deleteMany({})
}

async function main () {
  await clearDb()
  const informaticsBuilding = new Building({ name: 'Informatics', floor: 11 })
  // Room 3
  const room3c01 = new Room({ name: 'IF-3c01', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c01)
  const room3c02 = new Room({ name: 'IF-3c02', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c02)
  const room3c03 = new Room({ name: 'IF-3c03', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c03)
  const room3c04 = new Room({ name: 'IF-3c04', capacity: 60, floor: 3, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3c04)
  const room3m210 = new Room({ name: 'IF-3m210', capacity: 210, floor: 3, descs: ['Meeting room'], approver_order: 1, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room3m210)
  await informaticsBuilding.save()
  await room3c01.save()
  await room3c02.save()
  await room3c03.save()
  await room3c04.save()
  await room3m210.save()

  // Room 4
  const room4c01 = new Room({ name: 'IF-3c01', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c01)
  const room4c02 = new Room({ name: 'IF-3c02', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c02)
  const room4c03 = new Room({ name: 'IF-3c03', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c03)
  const room4c04 = new Room({ name: 'IF-3c04', capacity: 60, floor: 4, descs: ['computer', 'projector', 'OHP'], approver_order: 2, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4c04)
  const room4m210 = new Room({ name: 'IF-3m210', capacity: 210, floor: 4, descs: ['Meeting room'], approver_order: 1, building: informaticsBuilding })
  informaticsBuilding.rooms.push(room4m210)
  await informaticsBuilding.save()
  await room4c01.save()
  await room4c02.save()
  await room4c03.save()
  await room4c04.save()
  await room4m210.save()

  // Room for Test
  // const roomTest = new Room({ name: 'roomTest01', capacity: 60, floor: 4, descs: ['TestObject1', 'TestObject2', 'TestObject3'], approver_order: 2, building: informaticsBuilding })
  // informaticsBuilding.rooms.push(roomTest)
  // await informaticsBuilding.save()
  // await roomTest.save()
}

main().then(function () {
  console.log('Finish')
})
