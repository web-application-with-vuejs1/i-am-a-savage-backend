const mongoose = require('mongoose')
const User = require('../models/User')
const Admin = require('../models/Admin')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/Classroom_Reservations_Record')
async function clearUser () {
  await User.deleteMany({})
}
async function main () {
  await clearUser()

  const user1 = new User({ username: '62160244@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0864879521', roles: [ROLE.USER] })
  user1.save()

  const user2 = new User({ username: '62160252@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0698752149', roles: [ROLE.USER] })
  user2.save()

  const user3 = new User({ username: '62160277@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0875414785', roles: [ROLE.USER] })
  user3.save()

  const user4 = new User({ username: '62160310@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0698754124', roles: [ROLE.USER] })
  user4.save()

  const user5 = new User({ username: '62160315@go.buu.ac.th', password: 'password', faculty: 'com-sci', phone: '0647828457', roles: [ROLE.USER] })
  user5.save()

  const institutioAdministrator = new Admin({ username: 'admin1@mail.com', password: 'password', roles: [ROLE.INSTITUTIOAM] }) // ดูแลหน่วยงาน
  institutioAdministrator.save()

  const systemAdministrator = new Admin({ username: 'admin2@mail.com', password: 'password', roles: [ROLE.SYSTEM] })// ดูแลระบบ
  systemAdministrator.save()

  const approver1 = new Admin({ username: 'approver1@mail.com', password: 'password', roles: [ROLE.APPROVER] })//  พิจารณา1
  approver1.save()

  const approver2 = new Admin({ username: 'approver2@mail.com', password: 'password', roles: [ROLE.APPROVER] })//  พิจารณา2
  approver2.save()
}

main().then(function () {
  console.log('Finish')
})
