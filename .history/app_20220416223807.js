const express = require('express')
const cors = require('cors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')

const mongoose = require('mongoose')

const indexRouter = require('./routes/index')
const usersRouter = require('./routes/users')
const roomsRouter = require('./routes/rooms')
const adminsRouter = require('./routes/admins')
const authRouter = require('./routes/auth')
const eventRouter = require('./routes/events')
const buildingRouter = require('./routes/building')
const classroomBookingCertificateRouter = require('./routes/ClassroomBookingCertificate')
const dotenv = require('dotenv')
// const { authenMiddleware, authorizeMiddleware } = require('./helpers/auth')
// const { ROLE } = require('./constant')

// get config vars
dotenv.config()
mongoose.connect('mongodb://localhost:27017/Classroom_Reservations_Record')

const app = express()

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/', indexRouter)
app.use('/users', usersRouter)
app.use('/rooms', roomsRouter)
app.use('/auth', authRouter)
app.use('/building', buildingRouter)
app.use('/classroomBookingCertificate', classroomBookingCertificateRouter)
app.use('/eventRouter', eventRouter)
module.exports = app
