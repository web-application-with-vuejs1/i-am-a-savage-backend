const express = require('express')
const router = express.Router()
const Admin = require('../models/Admin')

const getAdmins = async function (req, res, next) {
  try {
    const admins = await Admin.find({}).exec()
    res.status(200).json(admins)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getAdmin = async function (req, res, next) {
  const id = req.params.id
  try {
    const admin = await Admin.findById(id).exec()
    if (admin === null) {
      res.status(404).json({
        message: 'Admin not found!!'
      })
    }
    res.json(admin)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addAdmin = async function (req, res, next) {
  const addAdmin = new Admin({
    username: req.body.username,
    password: req.body.password
    roles: [req.body.roles]
  })
  try {
    await addAdmin.save()
    res.status(201).json(addAdmin)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateAdmin = async function (req, res, next) {
  const adminId = req.params.id
  try {
    const admin = await Admin.findById(adminId)
    admin.password = req.body.password,
    admin.roles = req.body.roles
    await admin.save()
    return res.status(200).json(admin)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const deleteAdmin = async function (req, res, next) {
  const adminId = req.params.id
  try {
    await Admin.findByIdAndDelete(adminId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAdmins)
router.get('/:id', getAdmin)
router.post('/', addAdmin)
router.put('/:id', updateAdmin)
router.delete('/:id', deleteAdmin)
module.exports = router
