const express = require('express')
const router = express.Router()
const Admin = require('../models/Admin')

const getAdmins = async function (req, res, next) {
  try {
    const admins = await Admin.find({}).exec()
    res.status(200).json(admins)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getAdmin = async function (req, res, next) {
  const id = req.params.id
  try {
    const admin = await Admin.findById(id).exec()
    if (admin === null) {
      res.status(404).json({
        message: 'Admin not found!!'
      })
    }
    res.json(admin)
  } catch (err) {
    res.status(404).json({
      message: err.message
    })
  }
}

const addAdmin = async function (req, res, next) {
  const addAdmin = new Admin({
    password: req.body.password
  })
  try {
    await addAdmin.save()
    res.status(201).json(addAdmin)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

// const updateUser = async function (req, res, next) {
//   const userId = req.params.id
//   try {
//     const user = await User.findById(userId)
//     user.username = req.body.username,
//     user.password = req.body.password,
//     user.roles = req.body.roles
//     await user.save()
//     return res.status(200).json(user)
//   } catch (err) {
//     return res.status(404).send({ message: err.message })
//   }
// }

const deleteAdmin = async function (req, res, next) {
  const adminId = req.params.id
  try {
    await User.findByIdAndDelete(adminId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAdmins)
router.get('/:id', getUser)
router.post('/', addAdmin)
// router.put('/:id', updateUser)
router.delete('/:id', deleteUser)
module.exports = router
