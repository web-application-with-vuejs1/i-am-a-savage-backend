const express = require('express')
const router = express.Router()
const Building = require('../models/Building')

const getAll = async (req, res, next) => {
  try {
    // WORK IN PROGRESS(GET ALL Malfunction(Schema hasn't been registered for model \"Room\".\nUse mongoose.model(name, schema))
    const building = await Building.find({})
    res.status(200).json(building)
    console.log(building)
  } catch (err) {
    console.log('Error message : ' + err)
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBuildingByID = async (req, res, next) => {
  const id = req.params.id
  try {
    const building = await Building.findById(id).exec()
    if (building === null) {
      return res.status(404).send({
        message: 'Building not found'
      })
    }
    res.json(building)
    console.log(building)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const addBuilding = async (req, res, next) => {
  console.log('add Building')
  const newBuilding = new Building({
    name: req.body.name,
    floor: req.body.floor,
    rooms: req.body.rooms
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(201).send({
      message: err.message
    })
  }
}

const updateBuilding = async (req, res, next) => {
  console.log('update bulding')
  const buildingId = req.params.id
  try {
    const building = await Building.findById(buildingId)
    building.name = req.body.name
    building.floor = req.body.floor
    building.rooms = req.body.rooms
    await building.save()
    res.status(200)
  } catch (err) {
    return res.status(204).send({ message: err.message })
  }
}
const deleteBuilding = async (req, res, next) => {
  console.log('delete building')
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    console.log('Building deleted!')
    return res.status(200).send({ message: 'buliding deleted' })
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAll)
router.get('/:id', getBuildingByID)
router.post('/', addBuilding)
router.delete('/:id', deleteBuilding)
router.put('/:id', updateBuilding)
module.exports = router
