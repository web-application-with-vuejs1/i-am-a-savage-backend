const express = require('express')
const router = express.Router()
const User = require('../models/User')
const Admin = require('../models/Admin')
const { generateAccessToken } = require('../helpers/auth')
const bcrypt = require('bcryptjs')

const login = async function (req, res, next) {
  const username = req.body.username
  const password = req.body.password
  try {
    const admin = await Admin.findOne({ username: username }).exec()
    if (admin != null) {
      require('dotenv').config()
      // eslint-disable-next-line no-unused-expressions
      process.env.JWT_KEY
      console.log('Admin : ' + admin)
      const verifyResult = await bcrypt.compare(password, admin.password)
      if (!verifyResult) {
        console.log('Incorrect password (Admin)')
        res.status(404).json({
          message: 'Incorrect password(Admin)'
        })
      } const token = generateAccessToken({ _id: admin._id, username: admin.username })
      res.json({ user: { _id: admin._id, username: admin.username, roles: admin.roles }, token: token })
    } else {
      const user = await User.findOne({ username: username }).exec()
      if (user == null) {
        console.log('User not found!!')
        res.status(404).json({
          message: 'User not found!!'
        })
      }
      require('dotenv').config()
      // eslint-disable-next-line no-unused-expressions
      process.env.JWT_KEY
      console.log('user:' + user)
      const verifyResult = await bcrypt.compare(password, user.password)
      if (!verifyResult) {
        console.log('Incorrect password')
        res.status(404).json({
          message: 'Incorrect password!!'
        })
      }
      const token = generateAccessToken({ _id: user._id, username: user.username })
      res.json({ user: { _id: user._id, username: user.username, roles: user.roles }, token: token })
    }
  } catch (err) {
    console.log('Error message : ' + err)
  }
}
router.post('/login', login)
module.exports = router
