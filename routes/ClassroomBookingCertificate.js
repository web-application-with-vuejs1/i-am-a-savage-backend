const express = require('express')
const router = express.Router()
const ClassroomBookingCertificate = require('../models/ClassroomBookingCertificate')
// const Room = require('../models/Room')
// const User = require('../models/User')
// const Building = require('../models/Building')
// const Admin = require('../models/Admin')

const getClassroomBCs = async (req, res, next) => {
  try {
    const classroombcs = await ClassroomBookingCertificate.find({})
    res.status(200).json(classroombcs)
  } catch (err) {
    console.log('Error message : ' + err)
    return res.status(500).send({
      message: err.message
    })
  }
}
const deleteClassroomBC = async (req, res, next) => {
  const classroomBCId = req.params.id
  try {
    await ClassroomBookingCertificate.findByIdAndDelete(classroomBCId)
    return res.status(200).send({ message: 'deleted' })
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const getClassroomBC = async (req, res, next) => {
  const id = req.params.id
  try {
    const classroomBC = await ClassroomBookingCertificate.findById(id).exec()
    if (classroomBC === null) {
      return res.status(404).send({
        message: 'ClassroomBookingCertificate not found'
      })
    }
    res.json(classroomBC)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const addClassroomBC = async (req, res, next) => {
  const newClassRoomBC = new ClassroomBookingCertificate({
    room: req.body.room,
    // user: req.body.user,
    user: '62160244@go.buu.ac.th',
    startDate: req.body.startDate,
    endDate: req.body.endDate,
    // startDate: await eventDate.findById(req.body.startDate),
    // endDate: await eventDate.findById(req.body.endDate),
    approver: 'approver1@mail.com',
    building: req.body.building,
    bcStatus: 'Awaiting approval',
    note: req.body.note
  })
  try {
    // const events = await Event.find({
    //   $or: [{ startDate: { $gte: newClassRoomBC.startDate, $lt: newClassRoomBC.endDate } },
    //     { endDate: { $gte: newClassRoomBC.startDate, $lt: newClassRoomBC.ndDate } }]
    // }).exec()
    await newClassRoomBC.save()
    res.status(201).json(newClassRoomBC)// .json(events)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateClassroomBC = async (req, res, next) => {
  const ClassroomBCId = req.params.id
  try {
    const classroomBC = await ClassroomBookingCertificate.findById(ClassroomBCId)
    classroomBC.bcStatus = req.body.bcStatus
    await classroomBC.save()
    res.status(200).send(classroomBC)
    // if (Object.keys(req.body).length === 1) {
    //   console.log('req.body.approver are blank or null')
    //   const classroomBC = await ClassroomBookingCertificate.findById(ClassroomBCId)
    //   classroomBC.bcStatus = req.body.bcStatus
    //   await classroomBC.save()
    //   res.status(200).send(classroomBC)
    // } else {
    //   console.log('req.body.approver aren\'t blank or null')
    //   const classroomBC = await ClassroomBookingCertificate.findByIdAndUpdate({ _id: ClassroomBCId }, {
    //     $push: { approver: await Admin.findById(req.body.approver) }, $set: { bcStatus: req.body.bcStatus }
    //   })
    //   await classroomBC.save()
    //   res.status(200).send(classroomBC)
    // }
  } catch (err) {
    console.log(err)
    return res.status(204).send({ message: err.message })
  }
}

router.put('/:id', updateClassroomBC)
router.post('/', addClassroomBC)
router.get('/:id', getClassroomBC)
router.delete('/:id', deleteClassroomBC)
router.get('/', getClassroomBCs)
module.exports = router
// ClassroomBookingCertificate
