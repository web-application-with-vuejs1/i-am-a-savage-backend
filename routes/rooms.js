const express = require('express')
const router = express.Router()
const Room = require('../models/Room')
const Building = require('../models/Building')
const getRooms = async (req, res, next) => {
  try {
    // WORK IN PROGRESS(GET ALL Malfunction(Schema hasn't been registered for model \"Room\".\nUse mongoose.model(name, schema))
    const rooms = await Room.find({})
    res.status(200).json(rooms)
  } catch (err) {
    console.log('Error message : ' + err)
    return res.status(500).send({
      message: err.message
    })
  }
}
const getRoom = async (req, res, next) => {
  const id = req.params.id
  try {
    const room = await Room.findById(id).exec()
    if (room === null) {
      return res.status(404).send({
        message: 'Room not found'
      })
    }
    res.json(room)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const addRoom = async (req, res, next) => {
  const newRoom = new Room({
    name: req.body.name,
    floor: req.body.floor,
    capacity: req.body.capacity,
    descs: req.body.descs,
    approver_order: req.body.approver_order,
    // building: req.body.building
    building: await Building.findById(req.body.building).exec()
  })
  console.log('input building = ' + req.body.building)
  try {
    await newRoom.save()
    await Building.findByIdAndUpdate({ _id: req.body.building }, {
      $push: { rooms: await Room.find({ name: req.body.name }).exec() }
    })
    res.status(201).json(newRoom)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const deleteRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    await Room.findByIdAndDelete(roomId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

const updateRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)
    console.log('roomId = ' + roomId)
    room.name = req.body.name
    room.floor = req.body.floor
    room.capacity = req.body.capacity
    room.descs = req.body.descs
    room.approver_order = req.body.approver_order
    // const building = await Building.findById(room.building)
    // const buildingRoom = await building.rooms.find({ _id: roomId })
    // buildingRoom.name = req.body.name
    // buildingRoom.floor = req.body.floor
    // buildingRoom.capacity = req.body.capacity
    // buildingRoom.descs = req.body.descs
    // buildingRoom.approver_order = req.body.approver_order
    // await buildingRoom.save()
    // const buildingRoom = await building.room.findById(roomId)
    // room.building = req.body.building
    // building: await Building.findById(req.body.building).exec()
    // await Building.findByIdAndUpdate({ _id: req.body.building }, {
    //   $push: { rooms: await Room.find({ name: req.body.name }).exec() }
    // })
    await room.save()
    res.status(200).json(room)
    // res.status(200).json(room + buildingRoom)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.put('/:id', updateRoom)
router.delete('/:id', deleteRoom)
router.post('/', addRoom)
router.get('/:id', getRoom)
router.get('/', getRooms)
module.exports = router
