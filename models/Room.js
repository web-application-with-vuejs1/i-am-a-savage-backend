const mongoose = require('mongoose')
const { Schema } = mongoose
const roomSchema = Schema({
  name: String,
  floor: Number,
  capacity: { type: Number, default: 50 },
  descs: [String],
  approver_order: Number,
  building: { type: Schema.Types.ObjectId, ref: 'Building' }
})

module.exports = mongoose.model('Room', roomSchema)
