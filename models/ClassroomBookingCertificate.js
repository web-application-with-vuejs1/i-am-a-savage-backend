const mongoose = require('mongoose')
const { Schema } = mongoose
const ClassroomBookingCertificateSchema = Schema({
  room: String,
  building: String,
  startDate: String,
  endDate: String,
  user: String,
  approver: String,
  bcStatus: String,
  note: String
})

module.exports = mongoose.model('classroomBookingCertificate', ClassroomBookingCertificateSchema)
